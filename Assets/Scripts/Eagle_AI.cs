﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Eagle_AI : MonoBehaviour
{

    public Transform target;
    public float speed = 200f;
    public float nextWaypointDistance = 3f;
    public Transform sprite;

    Path path;
    int currentWaypoint = 0;
    bool reachedEndOfPath = false;

    Seeker seeker;
    Rigidbody2D rigidBody;

    // Start is called before the first frame update
    void Start()
    {

        seeker = GetComponent<Seeker>();
        rigidBody = GetComponent<Rigidbody2D>();

        InvokeRepeating( "UpdatePath", 0f, .5f );
        
    }

    void UpdatePath() {

        if( seeker.IsDone() ) {

            seeker.StartPath( rigidBody.position, target.position, OnPathComplete );
        }
        
    }

    void OnPathComplete(Path path) {

        if( !path.error ) {

            this.path = path;
            currentWaypoint = 0;
        }
    }

    // Update is called once per frame
    void FixedUpdate() {

        if(path == null ) {
            return;
        }

        if(currentWaypoint >= path.vectorPath.Count ) {

            reachedEndOfPath = true;
            return;

        } else {

            reachedEndOfPath = false;
        }

        Vector2 direction = ((Vector2) path.vectorPath[currentWaypoint] - rigidBody.position).normalized;
        Vector2 force = direction * speed * Time.deltaTime;

        rigidBody.AddForce( force );

        float distance = Vector2.Distance( rigidBody.position, path.vectorPath[currentWaypoint] );

        if(distance < nextWaypointDistance ) {

            currentWaypoint++;
        }

        if( force.x >= 0.01f ) {

            sprite.localScale = new Vector3( -1f, 1f, 1f );

        }else if( force.x <= -0.01f ) {

            sprite.localScale = new Vector3( 1f, 1f, 1f );
        }
    }
}
